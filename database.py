data_name = list()
data_id = list()
data_sn = list()
data_pn = list()
    #Наименование, ID, №Стелажа, №Полки
id_count = 0

run_programm = True
command = 0
while(run_programm):
    print("Выберите необходимое действие с базой:")
    print("1 - Найти\n2 - Добавить\n3 - Удалить")
    print("4 - Переместить\n0 - Закончить программу")
    command = int(input())
    if command == 0:
        run_programm = False
    if command == 1:
        subcommand = 0
        while(True):
            print("По какому параметру искать?\n1 - Имя\n2 - ID")
            subcommand = int(input())
            if (subcommand > 2 or subcommand < 1):
                print("Введите корректные данные!")
            else:
                break
        if (subcommand == 1):
            print("Начат поиск по имени!")
            #Тута ббудем искать по имени
            print("Введите по какому имени искать: ", end=" ")
            srch_name = input()
            ind = 0
            while (ind < id_count):
                if (data_name[ind] == srch_name):
                    print(f"Name: {data_name[ind]}; ID: {data_id[ind]};", end = " ")
                    print(f"SH: {data_sn[ind]}; PN: {data_pn[ind]}")
                ind = ind + 1
        if (subcommand == 2):
            print("Начат поиск по ID!")
            #А тута ббудем искать по ID
            print("Введите по какому ID искать: ", end=" ")
            srch_name = int(input())
            ind = 0
            while (ind < id_count):
                if (data_id[ind] == srch_name):
                    print(f"Name: {data_name[ind]}; ID: {data_id[ind]};", end = " ")
                    print(f"SH: {data_sn[ind]}; PN: {data_pn[ind]}")
                ind = ind + 1
    if command == 2:
        print("Введите наименование товара: ", end=" ")
        name = input()
        data_name.append(name) #Добавляем новый элемент с именем NAME
        data_id.append(id_count) #Добавляем новый элемент с ID ID_COUNT
        data_sn.append(0) #Добавляем новый элемент с стелажом по умолчанию
        data_pn.append(0) #Добаялем новый элемент с номер полк. по умолч.
        print(f"Имя товара: {data_name[id_count]}, ID: {data_id[id_count]}")
        id_count = id_count + 1
    if command == 4:
        print("Ввведите ID товара: ", end =" ")
        move_id = int(input())
        if move_id < id_count+1:
            print("Введите номер стелажа: ", end = " ")
            move_sn = int(input())
            print("Введите номер полки: ", end = " ")
            move_pn = int(input())
            data_sn[move_id] = move_sn
            data_pn[move_id] = move_pn
        else:
            print(f"ID {move_id} не существует!")